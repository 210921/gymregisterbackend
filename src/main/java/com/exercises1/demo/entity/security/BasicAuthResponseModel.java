package com.exercises1.demo.entity.security;

public class BasicAuthResponseModel  {
    private String message;

    public BasicAuthResponseModel(String message){
        this.message =message;
    }

    public BasicAuthResponseModel(){}

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
