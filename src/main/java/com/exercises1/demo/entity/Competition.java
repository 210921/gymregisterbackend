package com.exercises1.demo.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Competition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @ManyToOne
    @JoinColumn(name = "gym_id",referencedColumnName = "id")
    Gym competitor;
    String category;
    double kg;
    int age;
    LocalDate competitionDate;

    public Competition() {
    }

    public Competition(Gym competitor, String category, double kg, int age, LocalDate competitionDate) {
        this.competitor = competitor;
        this.category = category;
        this.kg = kg;
        this.age = age;
        this.competitionDate = competitionDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Gym getCompetitor() {
        return competitor;
    }

    public void setCompetitor(Gym competitor) {
        this.competitor = competitor;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getKg() {
        return kg;
    }

    public void setKg(double kg) {
        this.kg = kg;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public LocalDate getCompetitionDate() {
        return competitionDate;
    }

    public void setCompetitionDate(LocalDate competitionDate) {
        this.competitionDate = competitionDate;
    }
}
