package com.exercises1.demo;

import com.exercises1.demo.config.AppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}

}
