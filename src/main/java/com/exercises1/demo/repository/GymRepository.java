package com.exercises1.demo.repository;

import com.exercises1.demo.entity.Gym;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GymRepository extends JpaRepository<Gym,Integer> {
    Gym findByFirstname(String competitor);

}
