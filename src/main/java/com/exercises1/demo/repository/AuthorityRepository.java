package com.exercises1.demo.repository;

import com.exercises1.demo.entity.security.AuthorityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends JpaRepository<AuthorityEntity,Integer> {
}
