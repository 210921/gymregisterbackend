package com.exercises1.demo.controller;

import com.exercises1.demo.entity.Competition;
import com.exercises1.demo.entity.CompetitionDto;
import com.exercises1.demo.entity.Gym;
import com.exercises1.demo.repository.CompetitionRepository;
import com.exercises1.demo.repository.GymRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(path = "/competition")
public class CompetitionController {

    @Autowired
    private CompetitionRepository competitionRepository;

    @Autowired
    private GymRepository gymRepository;
    @GetMapping
    public List<Competition> getAllCompetitors (){
        return competitionRepository.findAll();
    }

    @GetMapping("/{id}")
    public CompetitionDto getCompetitorById(@PathVariable Integer id){
        Competition competition = competitionRepository.findById(id).orElseThrow(()->new RuntimeException("The competitor with id: " +id+ " is not found"));
        CompetitionDto competitionDto = new CompetitionDto(
                competition.getCompetitor().getFirstname(),
                competition.getCategory(),
                competition.getKg(),
                competition.getAge(),
                competition.getCompetitionDate()
        );

        return competitionDto;
    }

    @PostMapping
    public Competition addCompetitor(@RequestBody CompetitionDto competitionDto){
        Gym gym = gymRepository.findByFirstname(competitionDto.getCompetitor());
        Competition competition = new Competition(
                gym,competitionDto.getCategory(),
                competitionDto.getKg(),
                competitionDto.getAge(),
                competitionDto.getCompetitionDate());

        return competitionRepository.save(competition);
    }
    @PutMapping("/{id}")
    public CompetitionDto updateCompetitor(@RequestBody CompetitionDto competitionDto,@PathVariable Integer id){
     Gym gym = gymRepository.findByFirstname(competitionDto.getCompetitor());
        Competition competition = new Competition();

        competition.setId(id);
        competition.setCompetitor(gym);
        competition.setCategory(competitionDto.getCategory());
        competition.setKg(competitionDto.getKg());
        competition.setAge(competitionDto.getAge());
        competition.setCompetitionDate(competitionDto.getCompetitionDate());

         competitionRepository.save(competition);
         return competitionDto;
    }
    @DeleteMapping("/{id}")
    public void deleteCompetitor(@PathVariable Integer id){
         competitionRepository.deleteById(id);
    }


}
