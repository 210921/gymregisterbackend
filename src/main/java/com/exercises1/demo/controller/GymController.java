package com.exercises1.demo.controller;

import com.exercises1.demo.entity.Gym;
import com.exercises1.demo.repository.GymRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/gym")
@CrossOrigin
public class GymController {

    @Autowired
    private GymRepository gymRepository;

    @GetMapping
    public List<Gym> getAllSubscribers() {
        return gymRepository.findAll();
    }

    @PostMapping
    public Gym addSubscriber(@RequestBody Gym gym) {
        return gymRepository.save(gym);
    }

    @GetMapping("/{id}")
    public Gym getSubscriber(@PathVariable Integer id) {
        return gymRepository.findById(id).get();
    }

    @DeleteMapping("/{id}")
    public String deleteSubscriberById(@PathVariable Integer id) {
        gymRepository.deleteById(id);
        return "Subscriber was deleted";
    }

    @PutMapping("/{id}")
    public Gym updateSubscriberById(@PathVariable Integer id, @RequestBody Gym gym) {
        gym.setId(id);
        return gymRepository.save(gym);
    }




}
