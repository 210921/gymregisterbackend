package com.exercises1.demo.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com.exercises1.demo")
@EntityScan("com.exercises1.demo.entity")
@EnableJpaRepositories("com.exercises1.demo.repository")
public class AppConfig {
}
